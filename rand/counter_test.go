package rand

import (
	"testing"
)

func benchmarkHitCount(i int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		HitCount(i)
	}
}

func BenchmarkHitCount100(b *testing.B) {
	benchmarkHitCount(100, b)
}

func BenchmarkHitCount1000(b *testing.B) {
	benchmarkHitCount(1000, b)
}

func BenchmarkHitCount100000(b *testing.B) {
	benchmarkHitCount(100000, b)
}

func BenchmarkHitCount1000000(b *testing.B) {
	benchmarkHitCount(1000000, b)
}
